//! [![Latest Version](https://img.shields.io/crates/v/fluid.svg)](https://crates.io/crates/fluid)
//! [![Documentation](https://img.shields.io/badge/api-rustdoc-purple.svg)](https://docs.rs/fluid)
//! [![Lines of Code](https://tokei.rs/b1/gitlab/Boiethios/fluid-rs)](https://gitlab.com/Boiethios/fluid-rs/blob/master/src)
//!
//! `fluid` is a library to write human readable tests.
//!
//! This crate is in alpha state. It is fully usable, but one can (rightfully)
//! think that there are incomplete or missing features. The maintainer would
//! be very happy if he was given some feedback, issues, pull requests, etc.
//!
//! __________
//!
//! `fluid` is a crate to make the tests more readable and easier to write,
//! mainly inspired by [xUnit](https://xunit.github.io/).
//! It is still in an unstable state. Be adviced that in future alphas
//! — let alone in the release — it could/will be very different.
//!
//! Wiki
//! ----
//!
//! Detailed information is in the [wiki](https://docs.rs/fluid/latest/fluid/wiki/index.html).
//!
//! Goals
//! -----
//!
//! The current goals are to provide:
//!
//! - ☑ Easily readable tests: they should be read like english sentences.
//! - ☑ Nice and understandable error messages.
//! - ☐ The most possible useful assertions for common cases: numbers, `Iterators`, `Options`, `Results`, etc.
//! - ☐ A full test framework that launches the tests, displays the failures, etc.
//! Since this feature is not stabilized, it will be gated in the nightly compiler.
//!
//! Non-goals:
//!
//! - Run the tests fast: under the hood, there are some code generation at compile-time,
//! especially for the `#[theory]` and/or `#[session]` tests. This will never be as fast as the standard tests.
//!
//! Known bugs/limitations
//! ----------------------
//!
//! - The stringification of the left element is not perfect when the procedural macros are used.
//!     That is because there is no way to render the token as they were written by the user (as far as I know).
//! - Hygiene issue. For now, every span is located at the macro call site.
//!     That means that the tests report a failure at the `#[fact]`, `#[theory]` or `#[session]` line.
//!
//! Changelog
//! ---------
//!
//! See the [dedicated file](https://gitlab.com/Boiethios/fluid-rs/blob/master/CHANGELOG.md).
//!
//! Roadmap
//! ---------
//!
//! See the [dedicated file](https://gitlab.com/Boiethios/fluid-rs/blob/master/ROADMAP.md).

#![deny(unused)]
#![forbid(
    missing_docs,
    unsafe_code,
    missing_debug_implementations,
    bare_trait_objects
)]

mod assertions;
pub mod core;
pub mod prelude;
#[cfg(test)]
#[rustfmt::skip]
mod tests;
pub mod traits;
pub mod wiki;
