use colored::{ColoredString, Colorize};
use std::fmt::Debug;

static MAX_DISP_SIZE: Option<usize> = Some(32);

/*
 * PrintDebug
 */

/// Trait for an extension. See the method documentation.
pub trait PrintDebug {
    /// Display the user's input as a debug string.
    fn dbg(&self) -> String;
}

impl<T> PrintDebug for T
where
    T: Debug,
{
    fn dbg(&self) -> String {
        //let max = ::std::env::var(super::FLUID_MAX_SIZE)
        //    .and_then(|s| s.parse::<u32>())
        //    .unwrap_or(32);
        let s = format!("{:?}", self);
        if let Some(n) = MAX_DISP_SIZE {
            let len = s.chars().count();
            if len > n {
                let start = n * 3 / 4;
                let skip = len - n;
                let mut it = s.chars();
                let start: String = it.by_ref().take(start).collect();
                let end: String = it.skip(skip).collect();

                format!("{} (…) {}", strong(&start), strong(&end))
            } else {
                strong(&s).to_string()
            }
        } else {
            strong(&s).to_string()
        }
    }
}

/*
 * strong
 */

/// Colorize the input to emphatise it.
pub fn strong(s: &str) -> ColoredString {
    s.bright_yellow()
}

/*
 * Str
 */

/// Trait for an extension. See the method documentation.
pub trait Str {
    /// Convert a bool into a borrowed string as following:
    /// true → `""`
    /// false → `" not"`
    fn str(self) -> &'static str;
}

impl Str for bool {
    fn str(self) -> &'static str {
        if self {
            ""
        } else {
            " not"
        }
    }
}
