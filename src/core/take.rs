/// Trait to extends the defaultable types. See its method documentation.
pub trait Take: Sized {
    /// Replace `self` with its default value and returns the old one.
    fn take(&mut self) -> Self;
}

impl<T> Take for T
where
    T: Default + Sized,
{
    fn take(&mut self) -> Self {
        std::mem::replace(self, T::default())
    }
}
