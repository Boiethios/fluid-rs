use crate::core::should::ShouldImpl;
use std::fmt::Debug;

/// The trait used by the implementation detail, must be used by who wants to
/// add a custom assertion.
pub trait AssertionImpl: Sized {
    /// The left type.
    type Left: Debug;

    /// Returns a message if the assertion failed.
    fn failure_message(&mut self) -> Option<String>;

    /// Verify the assertion, and returns the `Should` struct.
    fn consume_as_should(self) -> ShouldImpl<Self::Left>;

    /// Add an explanation from the because method from the `Assertion` trait.
    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left>;
}
