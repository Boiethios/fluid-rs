use crate::assertions::string::contain::ContainString;
use crate::build::assert::ChainableAssert;
//use crate::traits::collection::Collection;
use std::fmt::Debug;

/*
/// Extension for Should<AsRef<str>>. See the method documentation.
pub trait ShouldCollection<C: Debug>
where
    C: Collection,
{
    /// Checks that a string contains a char.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// "Hello".should().contain('l');
    /// ```
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// "Hello".should().not().contain(|c: char| c.is_numeric());
    /// ```
    fn is_empty(&self) -> ChainableAssert;
    //fn contain(
    //    self,
    //    right: Collection::Item,
    //) -> ChainableAssert<crate::assertions::contain::ContainString<S, P>>
    //where
    //    P: StringPattern;
}
*/
