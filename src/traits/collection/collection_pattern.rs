use super::Collection;
use std::fmt::Debug;

/// See the method documentation.
pub trait CollectionPattern<C>
where
    C: Collection,
{
    /// Checks if a collection matches the pattern.
    fn matches(&self, collection: &C) -> bool;

    /// Displays the pattern.
    fn display(&self) -> String;
}

impl<C> CollectionPattern<C> for C::Item
where
    C: Collection,
    C::Item: Debug,
{
    fn matches(&self, collection: &C) -> bool {
        collection.contains(&self)
    }

    fn display(&self) -> String {
        format!("{:?}", self)
    }
}
