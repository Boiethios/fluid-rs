//! A `#[fact]` is a simple concrete fact to be verified. For example:
//!
//! ```rust
//! # use fluid::prelude::*;
//! #[fact]
//! fn cerberus_has_3_heads() {
//!     number_of_faces("Cerberus").should().be_equal_to(3)
//!         .because("that's how Cerberus is described");
//! }
//! ```
//!
//! If the test fails, it displays a nice error message:
//!
//! <style>
//! .bold {
//!     font-weight: bold;
//! }
//! .sg {
//!     color: yellow;
//! }
//! </style>
//! <pre style="color:azure; background-color:#444;">
//! failures:<br/>
//! ---- cerberus_has_3_heads stdout ----
//! <span style="color: red;">The test failed at <span class="bold">src/main.rs:5</span>:</span>
//! &nbsp;&nbsp;&nbsp;&nbsp;<span class="sg">number_of_faces ( "Cerberus" )</span> has the value <span class="sg">1</span>.
//! &nbsp;&nbsp;&nbsp;&nbsp;It should be equal to <span class="sg">3</span> but it is not.
//! This test should pass because that's how Cerberus is described.</pre>
//!
//! It is *possible* to use standard assertions inside of the `fact` function,
//! but it makes few sense, because the whole point of `fact` is that it can
//! print a prettified error message in association with `should()`.
//!
//! In contrast, if for one or another reason you do not want to use the custom
//! attribute, you can replace it with a call to the `fact_` macro:
//!
//! ```rust
//! # use fluid::prelude::*;
//! #[test]
//! fn cerberus_has_3_heads() {
//!     fact_!(number_of_faces("Cerberus")).should().be_equal_to(3)
//!         .because("that's how Cerberus is described");
//! }
//! ```
