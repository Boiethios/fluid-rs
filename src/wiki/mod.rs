//! The crate's wiki.
//!
//! Get started
//! -----------
//!
//! As a general advice, it should not be forgotten that everything related to
//! this crate must be used only within a test config
//! (*i.e.*, with a `#[cfg(test)]` attribute).
//! Otherwise, the library sources are mixed with the tests ones.
//!
//! Add the crate in the Cargo.toml as a dev dependency:
//!
//! ```toml
//! [dev-dependencies]
//! fluid = "0.4"
//! ```
//!
//! For those stuck in the 2015 edition, reference the crate in the main file:
//!
//! ```rust
//! #[cfg(test)] extern crate fluid;
//! ```
//!
//! Import the needed content in scope of the test files:
//!
//! ```rust
//! use fluid::prelude::*;
//! ```
//!
//! [The tests](assertions_list/index.html) can then be written:
//!
//! Facts and theories
//! ------------------
//!
//! ### Fact
//!
//!
//! ```rust
//! # use fluid::prelude::*;
//! # fn number_of_faces(_: &str) -> u8 { 3 }
//! #[fact]
//! fn cerberus_has_3_heads() {
//!     number_of_faces("Cerberus").should().be_equal_to(3);
//! }
//! ```
//!
//! [More about facts](fact/index.html).
//!
//! ### Theory
//!
//! ```rust
//! # use fluid::prelude::*;
//! #[theory]
//! #[case("Cerberus", 3)]
//! #[case("Hydra", 7)]
//! #[case("Janus", 2)]
//! #[case("Normal guy", 1)]
//! fn each_creature_has_a_correct_number_of_faces(name: &str, nbr_faces: u8) {
//!     number_of_faces(name).should().be_equal_to(nbr_faces);
//! }
//! ```
//!
//! [More about theories](theory/index.html).
//!
//! Setup and teardown
//! ------------------
//!
//! Tests sessions are available: they allow to add a setup, a teardown, and a context to the tests:
//!
//! ```rust
//! use fluid::prelude::*;
//!
//! struct FooTests {
//!     // The needed context.
//! }
//!
//! impl Default for FooTests {
//!     fn default() -> Self {
//!         // Here goes the setup.
//!         FooTests {
//!             // ...
//!         }
//!     }
//! }
//!
//! impl Drop for FooTests {
//!     fn drop(&mut self) {
//!         // Here goes the teardown.
//!     }
//! }
//!
//! // Here go the tests:
//! #[session]
//! impl FooTests {
//!     #[fact]
//!     fn dummy_example(self) {
//!         // The context can be used through `self` for the tests.
//!     }
//! }
//! ```
//!
//! [More about sessions](session/index.html).

pub mod assertions_list;
pub mod fact;
pub mod session;
pub mod theory;
