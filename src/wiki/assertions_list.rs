//! The complete list of the implemented assertions.
//!
//! Note that the assertions marked as consuming consume the left part of the assertion, so there cannot be a chain of multiple assertions.
//!
//! Those assertions print a more complete error message in the test functions marked as `#[fact]` or `#[attribute]`. They can be used in a standard `#[test]` function as well, but in this case, the location and the stringified version of the left side are not displayed in the failure message.
//!
//! ## Various features
//!
//! ### Negation
//!
//! ```rust
//! # use fluid::prelude::*;
//! (2 + 2).should().not().be_equal_to(5);
//! ```
//!
//! ### Chaining
//!
//! ```rust
//! # use fluid::prelude::*;
//! "42".parse::<i32>().should().not().be_an_error()
//!     .and_should().yield_the_item(42);
//! ```
//!
//! ### Explanation
//!
//! ```rust
//! # use fluid::prelude::*;
//! fn the_answer() -> i32 {
//!     42
//! }
//!
//! the_answer().should().be_equal_to(42).because("42 is the answer to the question");
//! ```
//!
//!
//! ## Equality
//!
//! ```rust
//! # use fluid::prelude::*;
//! (1 + 1).should().be_equal_to(2);
//! ```
//!
//! With precision (for floats only):
//!
//! ```rust
//! # use fluid::prelude::*;
//! #[cfg(feature = "float")]
//! (0.999 * 2.).should().be_equal_to(2.01).with_precision(0.1);
//! ```
//!
//!
//! ## `Iterator`s (consuming assertions)
//!
//! ### Checking if contain something
//!
//! ```rust
//! # use fluid::prelude::*;
//! Some(2).should().yield_the_item(2);
//! ```
//!
//! ```rust
//! # use fluid::prelude::*;
//! vec![1, 2, 3].into_iter().should().yield_the_item(1);
//! ```
//!
//! ## Collections
//!
//! ### Checking if empty
//!
//! ```rust
//! # use fluid::prelude::*;
//! let empty_list = std::collections::LinkedList::<i32>::new();
//!
//! empty_list.should().be_empty();
//! ```
//!
//! ### Checking if contain something
//!
//! ```rust
//! # use fluid::prelude::*;
//! let empty_list = std::collections::LinkedList::<i32>::new();
//!
//! vec![1, 2, 3].should().contain(1);
//! ```
//!
//!
//! ## `Result`s
//!
//! ### Checking if it is ok
//!
//! ```rust
//! # use fluid::prelude::*;
//! "123".parse::<i32>().should().be_ok();
//! ```
//!
//! Note that you can also use the iterator assertions.
//!
//! ### Checking if it is an error
//!
//! ```rust
//! # use fluid::prelude::*;
//! "?".parse::<i32>().should().be_an_error();
//! ```
//!
//!
//! ## Strings
//!
//! ### Checking if it is empty
//!
//! ```rust
//! # use fluid::prelude::*;
//! "".should().be_empty();
//! ```
//!
//! ### Checking if it contains something
//!
//! ```rust
//! # use fluid::prelude::*;
//! let sentence = "Hello world.";
//!
//! sentence.should().contain("Hello");
//! sentence.should().contain('.');
//! sentence.should().contain(['.', ' ']);
//! sentence.should().not().contain(char::is_numeric);
//! ```
//!
//! ### Checking if it starts with something
//!
//! ```rust
//! # use fluid::prelude::*;
//! let sentence = "Hello world.";
//!
//! sentence.should().start_with(char::is_alphabetic);
//! sentence.should().start_with("Hello");
//! ```
//!
//! ### Checking if it ends with something
//!
//! ```rust
//! # use fluid::prelude::*;
//! let sentence = "Hello world.";
//!
//! sentence.should().not().end_with(char::is_alphabetic);
//! sentence.should().end_with('.');
//! ```
//!
//!
//! ## Boolean
//!
//! ```rust
//! # use fluid::prelude::*;
//!
//! Some(0).is_some().should().be_true();
//! (2 + 2 == 5).should().be_false();
//! ```
//!
//!
//! ## Generic
//!
//! When there is no specific needed assertion:
//!
//! ```rust
//! # use fluid::prelude::*;
//! fn foo() -> i32 {
//!     42
//! }
//!
//! foo().should().have_the_property(|&n| n % 2 == 0)
//!     .because("the number must be even");
//! ```
