//! A `#[theory]` is a test more complex than a fact, that must be proven or inferred with multiple cases.
//!
//! Therefore, a theory must come with its multiple test `case`s, for example:
//!
//! ```rust
//! # use fluid::prelude::*;
//! # fn number_of_faces(name: &str) -> u8 {
//! #     match name {
//! #         "Cerberus" => 3,
//! #         "Hydra" => 7,
//! #         "Janus" => 2,
//! #         _ => 1,
//! #     }
//! # }
//! #[theory]
//! #[case("Cerberus", 3)]
//! #[case("Hydra", 7)]
//! #[case("Janus", 2)]
//! #[case("A regular dice", 6)]
//! #[case("Normal guy", 1)]
//! fn each_creature_has_a_correct_number_of_faces(name: &str, nbr_faces: u8) {
//!     number_of_faces(name).should().be_equal_to(nbr_faces);
//! }
//! ```
//!
//! If the test fails, it displays a nice error message:
//!
//! <style>
//! .bold {
//!     font-weight: bold;
//! }
//! .sg {
//!     color: yellow;
//! }
//! </style>
//! <pre style="color:azure; background-color:#444;">
//! failures:<br/>
//! ---- each_creature_has_a_correct_number_of_faces::case_A_regular_dice_6 stdout ----
//! <span style="color: red;">The test failed at <span class="bold">src/main.rs:14</span>, for the case <span class="bold">name = "A regular dice", nbr_faces = 6</span>:</span>
//! &nbsp;&nbsp;&nbsp;&nbsp;<span class="sg">number_of_faces ( name )</span> has the value <span class="sg">1</span>.
//! &nbsp;&nbsp;&nbsp;&nbsp;It should be equal to <span class="sg">6</span> but it is not.</pre>
//!
//! It is *possible* to use standard assertions inside of the `theory` function,
//! but it makes few sense, because without `should()`, there would be no
//! prettified error message nor case information in case of failed test.

#[cfg(test)]
mod test {

    use crate::prelude::*;
    fn number_of_faces(name: &str) -> u8 {
        match name {
            "Cerberus" => 3,
            "Hydra" => 7,
            "Janus" => 2,
            _ => 1,
        }
    }

    #[theory]
    #[case("Cerberus", 3)]
    #[case("Hydra", 7)]
    #[case("Janus", 2)]
    //#[case("A regular dice", 6)]
    #[case("Normal guy", 1)]
    fn each_creature_has_a_correct_number_of_faces(name: &str, nbr_faces: u8) {
        number_of_faces(name).should().be_equal_to(nbr_faces);
    }

}
