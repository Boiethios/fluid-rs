use self::fluid::core::prelude::*;
use crate as fluid;
use std::ops::Not;

/// Used to check if a result is an error.
#[derive(Debug, Drop)]
pub struct ContainIterator<I: Debug>
where
    I: Iterator,
    I::Item: PartialEq<I::Item> + Debug,
{
    should: ShouldImpl<I>,
    right: I::Item,
}

impl<I: Debug> AssertionImpl for ContainIterator<I>
where
    I: Iterator,
    I::Item: PartialEq<I::Item> + Debug,
{
    type Left = I;

    fn failure_message(&mut self) -> Option<String> {
        let mut left = self.should.left.take()?;
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if left.any(|item| item == self.right) != self.should.truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} does{} contain {}: {}\n\
                     \tbut it should{}.",
                    stringified,
                    truthness.not().str(),
                    self.right.dbg(),
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!(
                    "\t{} does{} contain {}.",
                    left_dbg,
                    truthness.not().str(),
                    self.right.dbg()
                )
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}

impl<I: Debug> Should<I>
where
    I: IntoIterator,
    I::Item: PartialEq<I::Item> + Debug,
    I::IntoIter: Debug,
{
    /// Checks if an iterator contains the right field.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// let it = vec![1, 2, 3, 4].into_iter();
    ///
    /// it.should().not().yield_the_item(0);
    /// ```
    pub fn yield_the_item(self, right: I::Item) -> ConsumingAssert<ContainIterator<I::IntoIter>> {
        let implem = ShouldImpl::from(self);
        let implem = ContainIterator {
            should: implem.map_left(|i| Some(i.into_iter())),
            right,
        };

        ConsumingAssert(implem)
    }
}

#[cfg(test)]
mod tests {
    use crate::prelude::*;

    #[fact]
    fn slice_yield_assertion() {
        let it = [1, 2, 3, 4].iter();

        it.should().yield_the_item(&1);
    }

    #[fact]
    fn option_yield_assertion() {
        let option = Some(0);

        option.should().yield_the_item(0);
    }

    #[fact]
    fn result_yield_assertion() {
        let result: Result<i32, _> = "42".parse();

        result.should().yield_the_item(42);
    }
}
