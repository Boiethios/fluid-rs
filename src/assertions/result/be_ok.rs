use self::fluid::core::prelude::*;
use crate as fluid;
use std::ops::Not;

/// Used to check if a result is an error.
#[derive(Debug, Drop)]
pub struct BeOk<O: Debug, E: Debug> {
    pub(in crate::assertions) should: ShouldImpl<Result<O, E>>,
    pub(in crate::assertions) containing_truthness: bool,
}

impl<O: Debug, E: Debug> AssertionImpl for BeOk<O, E> {
    type Left = Result<O, E>;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if self.should.left.as_ref()?.is_ok() != self.should.truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} is {}\n\
                     \tbut it should{} be ok.",
                    stringified,
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!("\t{} is{} ok.", left_dbg, truthness.not().str())
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}

impl<O: Debug, E: Debug> Should<Result<O, E>> {
    /// Checks if a result is an error.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// let result = "2".parse::<i32>();
    /// result.should().be_ok();
    /// ```
    pub fn be_ok(self) -> ChainableAssert<BeOk<O, E>> {
        let implem = BeOk {
            should: self.into(),
            containing_truthness: true,
        };

        ChainableAssert(implem)
    }
}
