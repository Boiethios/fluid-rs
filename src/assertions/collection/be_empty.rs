use self::fluid::core::prelude::*;
use self::fluid::traits::collection::*;
use crate as fluid;
use std::ops::Not;

#[derive(Debug, Drop)]
pub struct BeEmptyCollection<C: Debug>
where
    C: Collection,
{
    pub(super) should: ShouldImpl<C>,
}

impl<C: Debug> AssertionImpl for BeEmptyCollection<C>
where
    C: Collection,
{
    type Left = C;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if self.should.left.as_ref()?.is_empty() != truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} has the value {}.\n\
                     \tIt is{} empty but it should{}.",
                    stringified,
                    left_dbg,
                    truthness.not().str(),
                    truthness.str(),
                )
            } else {
                format!("\t{} is{} empty.", left_dbg, truthness.not().str())
            };

            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}

#[cfg(test)]
mod test {
    use crate::prelude::*;

    #[fact]
    #[should_panic]
    fn empty() {
        let v = vec![1, 2, 3];

        v.should().be_empty();
    }
}
