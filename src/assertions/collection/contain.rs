use self::fluid::core::prelude::*;
use self::fluid::traits::collection::*;
use crate as fluid;
use std::ops::Not;

#[derive(Debug, Drop)]
pub struct ContainCollection<C: Debug, P>
where
    C: Collection,
    P: CollectionPattern<C>,
{
    pub(super) should: ShouldImpl<C>,
    pub(super) right: P,
}

impl<C: Debug, P> AssertionImpl for ContainCollection<C, P>
where
    C: Collection,
    P: CollectionPattern<C>,
{
    type Left = C;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if self.right.matches(self.should.left.as_ref()?) != truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} does{} contain {}: {}\n\
                     \tbut it should{}.",
                    stringified,
                    truthness.not().str(),
                    self.right.display(),
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!(
                    "\t{} does{} contain {}.",
                    left_dbg,
                    truthness.not().str(),
                    self.right.display()
                )
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}
