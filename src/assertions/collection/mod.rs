mod be_empty;
mod contain;

pub use be_empty::BeEmptyCollection;
pub use contain::ContainCollection;

use crate::core::prelude::*;
use crate::traits::collection::*;
use std::fmt::Debug;

impl<C: Debug> ShouldCollection<C> for Should<C>
where
    C: Collection,
{
    fn contain<P>(self, right: P) -> ChainableAssert<ContainCollection<C, P>>
    where
        P: CollectionPattern<C>,
    {
        let implem = ContainCollection {
            should: self.into(),
            right,
        };

        ChainableAssert(implem)
    }

    fn be_empty(self) -> ChainableAssert<BeEmptyCollection<C>> {
        let implem = BeEmptyCollection {
            should: self.into(),
        };

        ChainableAssert(implem)
    }
}
