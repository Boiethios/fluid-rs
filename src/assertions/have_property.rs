use self::fluid::core::prelude::*;
use crate as fluid;
use std::ops::Not;

/// Used to check if left has some property.
#[derive(Debug, Drop)]
pub struct HaveProperty<L: Debug, F>
where
    F: FnOnce(&L) -> bool,
{
    pub(crate) should: ShouldImpl<L>,
    pub(crate) closure: Option<F>,
}

impl<L: Debug, F> AssertionImpl for HaveProperty<L, F>
where
    F: FnOnce(&L) -> bool,
{
    type Left = L;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();
        let closure = self.closure.take()?;

        if closure(self.should.left.as_ref()?) != truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} does{} match the given property: {}\n\
                     \tbut it should{}.",
                    stringified,
                    truthness.not().str(),
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!(
                    "\t{} does{} match the given property.",
                    left_dbg,
                    truthness.not().str()
                )
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}

impl<L: Debug> Should<L> {
    /// Checks if a user-defined property is satisfied.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// fn an_even_number() -> i32 { 2 }
    ///
    /// an_even_number().should().have_the_property(|&n| n % 2 == 0);
    /// ```
    pub fn have_the_property<F>(self, closure: F) -> ChainableAssert<HaveProperty<L, F>>
    where
        F: FnOnce(&L) -> bool,
    {
        let implem = HaveProperty {
            should: self.into(),
            closure: Some(closure),
        };

        ChainableAssert(implem)
    }
}
