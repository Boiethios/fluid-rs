use self::fluid::core::prelude::*;
use self::fluid::traits::string::StringPattern;
use crate as fluid;
use std::ops::Not;

#[derive(Debug, Drop)]
pub struct ContainString<S: Debug, P>
where
    S: AsRef<str>,
    P: StringPattern,
{
    pub(super) should: ShouldImpl<S>,
    pub(super) right: P,
}

impl<S: Debug, P> AssertionImpl for ContainString<S, P>
where
    S: AsRef<str>,
    P: StringPattern,
{
    type Left = S;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if self.right.matches(self.should.left.as_ref()?.as_ref()) != truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} does{} contain {}: {}\n\
                     \tbut it should{}.",
                    stringified,
                    truthness.not().str(),
                    self.right.display(),
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!(
                    "\t{} does{} contain {}.",
                    left_dbg,
                    truthness.not().str(),
                    self.right.display()
                )
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}
