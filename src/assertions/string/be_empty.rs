use self::fluid::core::prelude::*;
use crate as fluid;
use std::ops::Not;

#[derive(Debug, Drop)]
pub struct BeEmptyString<S: Debug>
where
    S: AsRef<str>,
{
    pub(super) should: ShouldImpl<S>,
}

impl<S: Debug> AssertionImpl for BeEmptyString<S>
where
    S: AsRef<str>,
{
    type Left = S;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if self.should.left.as_ref()?.as_ref().is_empty() != truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} is {}\n\
                     \tbut it should{} be empty.",
                    stringified,
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!("\t{} is{} empty.", left_dbg, truthness.not().str())
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}
