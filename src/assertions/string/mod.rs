mod be_empty;
mod contain;
mod end_with;
mod start_with;

pub use be_empty::BeEmptyString;
pub use contain::ContainString;
pub use end_with::EndWithString;
pub use start_with::StartWithString;

use crate::core::prelude::*;
use crate::traits::string::{ShouldString, StringPattern};
use std::fmt::Debug;

impl<S: Debug> ShouldString<S> for Should<S>
where
    S: AsRef<str>,
{
    fn contain<P>(self, right: P) -> ChainableAssert<ContainString<S, P>>
    where
        P: StringPattern,
    {
        let implem = ContainString {
            should: self.into(),
            right,
        };

        ChainableAssert(implem)
    }

    fn start_with<P>(self, right: P) -> ChainableAssert<StartWithString<S, P>>
    where
        P: StringPattern,
    {
        let implem = StartWithString {
            should: self.into(),
            right,
        };

        ChainableAssert(implem)
    }

    fn end_with<P>(self, right: P) -> ChainableAssert<EndWithString<S, P>>
    where
        P: StringPattern,
    {
        let implem = EndWithString {
            should: self.into(),
            right,
        };

        ChainableAssert(implem)
    }

    fn be_empty(self) -> ChainableAssert<BeEmptyString<S>> {
        let implem = BeEmptyString {
            should: self.into(),
        };

        ChainableAssert(implem)
    }
}
