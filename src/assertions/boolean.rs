use self::fluid::core::prelude::*;
use crate as fluid;
use std::ops::Not;

/// Used to check if a result is an error.
#[derive(Debug, Drop)]
pub struct Boolean {
    should: ShouldImpl<bool>,
}

impl AssertionImpl for Boolean {
    type Left = bool;

    fn failure_message(&mut self) -> Option<String> {
        let left_dbg = self.should.left_dbg();
        let truthness = self.should.truthness();

        if self.should.left.as_ref()? != &truthness {
            let message = if let Some(stringified) = self.should.stringified() {
                format!(
                    "\t{} is {}\n\
                     \tbut it should{}.",
                    stringified,
                    left_dbg,
                    truthness.str()
                )
            } else {
                format!("\t{} is not {}.", left_dbg, truthness)
            };
            Some(message)
        } else {
            None
        }
    }

    fn consume_as_should(mut self) -> ShouldImpl<Self::Left> {
        self.should.take()
    }

    fn should_mut(&mut self) -> &mut ShouldImpl<Self::Left> {
        &mut self.should
    }
}

impl Should<bool> {
    /// Checks if a bool is true.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// let is_ok = true;
    /// is_ok.should().be_true();
    /// ```
    pub fn be_true(self) -> ChainableAssert<Boolean> {
        let implem = Boolean {
            should: self.into(),
        };

        ChainableAssert(implem)
    }

    /// Checks if a bool is false.
    ///
    /// # Example
    ///
    /// ```rust
    /// # use fluid::prelude::*;
    /// let is_ok = false;
    /// is_ok.should().be_false();
    /// ```
    pub fn be_false(self) -> ChainableAssert<Boolean> {
        let implem = Boolean {
            should: self.not().into(),
        };

        ChainableAssert(implem)
    }
}
