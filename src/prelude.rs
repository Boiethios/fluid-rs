//! The imports needed to use the crate.
//!
//! # Example
//!
//! ```
//! use fluid::prelude::*;
//! ```

pub use crate::core::should::{LeftElement, ShouldExtension};
pub use crate::traits::{collection::*, string::*};
pub use fluid_attributes::*;
pub use std::ops::Not;

/// A macro to replace the custom `#[fact]` attribute if you do not want to use it.
#[macro_export]
macro_rules! fact_ {
    ($e:expr) => {
        LeftElement::new($e, stringify!($e), concat!(file!(), ":", line!()), None)
    };
}
pub use crate::fact_;
