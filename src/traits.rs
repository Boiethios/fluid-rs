//! Various traits used for make the assertions generic.

/// The traits to abstract over the tests about collections.
pub mod collection;

/// The traits to abstract over the tests about strings.
pub mod string;
