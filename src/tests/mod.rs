//! This mod contains the actual unit tests.

//mod message;
mod session;

use crate::prelude::*;

#[fact]
fn simple_test() {
    (1 + 1).should().be_equal_to(2);
}

#[fact]
#[should_panic]
fn simple_test_fails() {
    (1 + 1).should().be_equal_to(3);
}

#[fact]
fn test_with_explanation() {
    (1 + 1).should().be_equal_to(2).because("it should work");
}

#[fact]
#[should_panic]
fn test_with_explanation_fails() {
    (1 + 1).should().be_equal_to(3).because("it should work");
}

#[fact]
fn chained_test() {
    (1 + 1).should().be_equal_to(2).and_should().be_equal_to(2);
}

#[fact]
#[should_panic]
fn chained_test_first_fails() {
    (1 + 1).should().be_equal_to(3).and_should().be_equal_to(2);
}

#[fact]
#[should_panic]
fn chained_test_second_fails() {
    (1 + 1).should().be_equal_to(2).and_should().be_equal_to(3);
}

#[fact]
fn simple_negated_test() {
    (1 + 1).should().not().be_equal_to(3);
}

#[fact]
#[should_panic]
fn simple_negated_test_fails() {
    (1 + 1).should().not().be_equal_to(2);
}

#[fact]
fn chained_first_negated_test() {
    (1 + 1).should().not().be_equal_to(3)
        .and_should().be_equal_to(2);
}

#[fact]
fn chained_second_negated_test() {
    (1 + 1).should().be_equal_to(2)
        .and_should().not().be_equal_to(3);
}

#[fact]
fn chained_both_negated_test() {
    (1 + 1).should().not().be_equal_to(3)
        .and_should().not().be_equal_to(3);
}

#[fact]
#[should_panic]
fn chained_negated_test_first_fails() {
    (1 + 1).should().not().be_equal_to(2)
        .and_should().be_equal_to(2);
}

#[fact]
#[should_panic]
fn chained_negated_test_second_fails() {
    (1 + 1).should().be_equal_to(2)
        .and_should().not().be_equal_to(2);
}

#[theory]
#[case(0)]
#[case(2)]
#[case(4)]
fn is_even(i: i32) {
    (i % 2).should().be_equal_to(0);
}
