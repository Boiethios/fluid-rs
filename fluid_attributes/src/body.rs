//! Handles the body generation, more specifically, the transformation of
//! the left side of the assertion into an `LeftElement` struct
//! that carries more information.

// ----------------------
//
// a.b().should().be_equal_to(b);
//
//     receiver:        method:
// 1.  a.b().should()   be_equal_to
// 2.  a.b()            should

use quote::{quote, ToTokens};
use syn::{
    parse_quote,
    visit_mut::{visit_expr_method_call_mut, VisitMut},
    ExprMethodCall, Ident,
};

pub struct Transform {
    case_ident: Option<Ident>,
}

impl Transform {
    pub fn new(case_ident: Option<Ident>) -> Self {
        Transform { case_ident }
    }
}

impl VisitMut for Transform {
    fn visit_expr_method_call_mut(&mut self, expr: &mut ExprMethodCall) {
        if expr.method == "should" {
            let left = &mut expr.receiver;
            let stringified = left.clone().into_token_stream().to_string();
            let case_ident = match &self.case_ident {
                Some(ident) => quote!(Some(#ident)),
                None => quote!(None),
            };

            debug!(stringified);
            *left = parse_quote! {
                LeftElement::new(#left, #stringified, concat!(file!(), ":", line!()), #case_ident)
            };
        } else {
            visit_expr_method_call_mut(self, expr)
        }
    }
}
