# Code generation: `theory`

## Without `session`

```rust
#[theory]
#[case(0)]
fn dummy(i: i32) {
    i.should().be_equal_to(0);
}
```

Generates:

```rust
fn dummy(i: i32, case_UUID: &'static str) {
    LeftElement::new(i, "i", "file:line", Some(case_UUID)).should().be_equal_to(0);
}

mod dummy {
    use super::*;

    #[test]
    pub fn case_0() {
        dummy(0, "i = 0")
    }
}
```

## Inside `session`

```rust
#[session]
impl Foo {
    #[theory]
    #[case(0)]
    fn dummy(self, i: i32) {
        i.should().be_equal_to(self.i);
    }
}
```

Generates:

```rust
impl Foo {
    fn dummy(self, i: i32, case_UUID: &'static str) {
        LeftElement::new(i, "i", "file:line", Some(case_UUID)).should().be_equal_to(self.i);
    }
}

mod dummy {
    use super::*;

    #[test]
    pub fn case_0() {
        Foo::default().dummy(0, "i = 0")
    }
}
```