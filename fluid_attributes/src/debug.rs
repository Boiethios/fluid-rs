/// Macro used to debug the generated tokens. Add the `"debug"` feature to display those.

macro_rules! debug {
    ($e:expr) => {
        #[cfg(feature = "debug")]
        {
            let title = format!(" {} ", stringify!($e));
            println!("\x1B[32m{:=^40}", title);
            println!("{}", $e);
            println!("\x1B[32m========================================");
        }
    };
}
