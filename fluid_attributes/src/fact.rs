use crate::{body, helpers::add_test_attribute};
use pm::TokenStream;
use quote::ToTokens;
use syn::{parse::Error, visit_mut::VisitMut, ItemFn};

pub fn generate(mut input: ItemFn) -> Result<TokenStream, Error> {
    add_test_attribute(&mut input.attrs);

    body::Transform::new(None).visit_block_mut(&mut input.block);

    Ok(input.into_token_stream())
}
