use pm::TokenStream;
use quote::{quote, ToTokens};
use syn::{parse::Error, parse_quote, GenericParam, ItemImpl, ItemStruct};

pub fn generate(item: ItemStruct) -> Result<TokenStream, Error> {
    let implem_type = {
        let implem_type = item.ident;
        let mut g = item.generics.params.clone();
        for param in g.iter_mut() {
            if let GenericParam::Type(t) = param {
                t.colon_token = None;
                t.bounds = Default::default();
                t.eq_token = None;
                t.default = None;
            }
        }
        parse_quote!(#implem_type<#g>)
    };

    let output = ItemImpl {
        attrs: Default::default(),
        defaultness: None,
        unsafety: None,
        impl_token: Default::default(),
        generics: item.generics.clone(),
        trait_: Some((None, parse_quote!(std::ops::Drop), Default::default())),
        self_ty: Box::new(implem_type),
        brace_token: Default::default(),
        items: vec![parse_quote! {
            fn drop(&mut self) {
                if let Some(msg) = fluid::core::prelude::AssertionImpl::failure_message(self) {
                    fluid::core::prelude::AssertionImpl::should_mut(self)
                        .add_message(msg);
                }
            }
        }],
    };

    Ok(output.into_token_stream())
}
