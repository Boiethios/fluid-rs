# Changelog

0.4.0
-----

- The `theory` attribute generates a module with one function per case.
- There is now the test `#[session]` that handles test setup, teardown and context.
- This is much easier to create a new assertion. The information about this is in the [`core` module](https://docs.rs/fluid/core/index.html).
- Much more assertions! See the wiki in the documentation for more information.
- The wiki is now [in the generated documentation](https://docs.rs/fluid/wiki/index.html).

0.3.0
-----

- Custom attributes:
    - `#[fact]`
    - `#[attribute]` + `#[case]`s
- Less assertions. A lot of assertions are planed, but only after some feedback. Rewrite all the assertions in a different manner at each alpha would be terrible.
- The `theory!` macro is now named `fact_!`, but the attribute `#[fact]` is preferred.
