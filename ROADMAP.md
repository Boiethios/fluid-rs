# Roadmap

## Generic

- Diff the debug values in case of failure because
for now, the long debug strings are cut without intelligence.
- Extract the core module inside its own crate.
- Standardize the way that failure message are displayed. For now, people can be confused by the wording.

## Assertions

- Iterator comparison
- `be_ok` and `be_an_error` unwrap the content and other assertions can be chained.
- More stuff for the collection; add a `Dictionary` trait to handle the corresponding assertions.
